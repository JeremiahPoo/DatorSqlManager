<?php 
/**
 * 
 * @author Quentin Gary 
 * 
 */
class TableCreator extends DatorConnector {

    protected $config; 
    protected $columns = [];
    protected $query;
    protected $table;

    public function __construct($table) {
        $this->setConfig($this->arrayInclude("config.php"));
        $this->setTable($table);
    }
    /**
     * @param string $file : the path of the config file 
     * @return object $config : the config
     * 
     */
    private function arrayInclude($file){
        include $file;
        return $config;    
    }
    /**
     * add a column to the table 
     * @param string $name : the name of the column 
     * @param string $type : the type of the column (int, string, etc ...)
     * @param int $length : the length of the column 
     * @param string $default : the default value for the column 
     * @param bool $primary : if the column is primary 
     * @param bool $autoIncrement : if the column auto increment 
     * @param array $foreignKey : if the column has a foreign key, give her a associative array here as argument where the key
     * is the other table, and the value the column of the other table 
     * @return self 
     */
    public function addColumn($name, $type, $length = 255 , $default = null ,$primary = false, $autoIncrement = false, $foreignKey = []) {

        ClassHelper::TypeException($name, ClassHelper::_STR); 
        ClassHelper::TypeException($type, ClassHelper::_STR);
        ClassHelper::TypeException($length, ClassHelper::_INTEGER);
        ClassHelper::TypeException($primary, ClassHelper::_BOOL);
        ClassHelper::TypeException($autoIncrement, ClassHelper::_BOOL);
        ClassHelper::TypeException($foreignKey, ClassHelper::_ARRAY);

        $this->columns[] = array(
            "name" => $name,
            "type" => $type,
            "length" => $length, 
            "primary" => $primary,
            "auto" => $autoIncrement,
            "default" => $default,
            "foreignKey" => $foreignKey
        );

        return $this;
    }
    /**
     * create the query and send it in $this->query 
     * 
     */
    public function createQuery() {
        $query = 'CREATE TABLE `'.$this->table.'` (';
        $primary = "";
        $auto = array();
        $index = 0;
        $foreignKey = array();
        $foreignQuery = "";
        foreach($this->columns as $col) {
            $typeLn = "";
            $collate = "";
            $query .= "`".$col["name"]."` ".$col["type"]; 
            if($col["type"] != "date") {
                $typeLn = " (".$col["length"].") ";
            }
            
            $default = $col["default"] == null ? "NULL" : $col["default"]; 

            $collate = " COLLATE utf8mb4_bin DEFAULT ".$default." ";
            $query .= $typeLn.$collate;

            if($col["primary"] == true) {
                if(empty($primary)) {
                    $primary = $col["name"];
                } else {
                    throw new Exception("You must have only one primary key");
                }
                
            }

            if($col["auto"] == true) {

                if(empty($auto)) {
                    $auto[] = $col;
                } 
                
            }

            if(!empty($col["foreignKey"])) {
                $foreignKey[$col["name"]] = $col["foreignKey"]; 
            }

            if($index != count($this->columns) - 1) {
                $query .= ", ";
            }
            $index++;
        }

        $query .= ') ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;';

        if(!empty($primary)) {
            $primaryQuery = " ALTER TABLE `".$this->table."`
            ADD PRIMARY KEY (`".$primary."`);";
        }

        if(!empty($auto)) {

            foreach($auto as $a) {
                $autoQuery = " ALTER TABLE `".$this->table."`
                MODIFY `".$a["name"]."` int(".$a["length"].") NOT NULL AUTO_INCREMENT;";
            }

        }

        if(!empty($foreignKey)) {
            foreach($foreignKey as $key => $value) {
                foreach($foreignKey[$key] as $table => $column ) {
                    $foreignQuery .= " ALTER TABLE `".$this->table."`
                    ADD FOREIGN KEY (".$key.") REFERENCES `".$table."`(".$column.");";
                }
            }
        }

        $indexQuery = 0;

        if(isset($query)) {
            $this->query[$indexQuery] = $query;
            $indexQuery++;
        }

        if(isset($primaryQuery)) {
            $this->query[$indexQuery] = $primaryQuery;
            $indexQuery++;
        }

        if(isset($autoQuery)) {
            $this->query[$indexQuery] = $autoQuery;
            $indexQuery++;
        }

        if(isset($foreignQuery) && $foreignQuery != "") {
            $this->query[$indexQuery] = $foreignQuery;
            $indexQuery++;
        }

        

    }
    /**
     * call the query and create the table 
     * @return bool : if the table has been successfully created 
     */
    public function createTable() {
        if($this->connect()) {
            $this->createQuery();
            $query = $this->query;
       
            foreach($query as $q) {
                $stmt = $this->db->prepare($q);

                if($stmt->execute()) {
                    
                } else {
                    return false;
                }
            }   

            return true; 

        } else {
            throw new Exception("Error when tying to connect to the database");
        }

        
    }


    /**
     * Get the value of table
     */ 
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Set the value of table
     *
     * @return  self
     */ 
    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }

    /**
     * Get the value of config
     */ 
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set the value of config
     *
     * @return  self
     */ 
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }
}