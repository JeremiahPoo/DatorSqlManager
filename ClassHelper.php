<?php 
/**
 * 
 * WIP 
 * 
 * @version 0.2
 * @author Quentin Gary 
 * 
 * 
 */
class ClassHelper {

     const _INTEGER = "int";
     const _INT = "int";
     const _STR = "string";
     const _ARRAY = "array"; 
     const _BOOL = "boolean";
     const _FUNCTION = "function";
     const _LINE = "line";
     const _FILE = "file";
     const _FLOAT = "float";
     const _DOUBLE = "double";

    public function __construct() {

    }
    /**
     * 
     * create exception if value is not to the type type
     * 
     * @param mixed the argument value 
     * @param string $type : the type to check 
     * @param string $function : the function where the exception was called. Decrecated right now, use of debug_backtrace
     * 
     * @throw Exception 
     * 
     * 
     */
    public static function TypeException($value , $type) {
        if(!self::is_type($value, $type)) {
            $realtype = gettype($value);
            $function = self::getOriginOffCall(self::_FUNCTION, 1);
            $file = self::getOriginOffCall(self::_FILE, 1);
            throw new Exception("The argument passed in ".$function." must be ".$type.". ".$realtype." received in ".$file);
        }
    }
    /**
     * 
     * get the origin of the function call 
     * 
     * @param int $resursion : the level of recursion of your function. 
     * 
     * if you want to know some informations about the origin of the call.
     * 
     * @return string $info : the nummer of LINE where the function was called 
     * 
     * or the name of the function 
     * 
     */
    public static function getOriginOffCall( $info = self::_FUNCTION , $recursion = 0 ) {
        
        $info = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS)[$recursion+1][$info];

        return $info;
    }


    /**
     * check if $value is type $type
     * 
     * @param mixed $value : the value you want to verify
     * @param string $type : the type to check 
     * 
     * @return bool  
     */
    protected static function is_type($value , $type) {
        switch($type) {
            case self::_INTEGER :
                return is_int($value);
                break;
            case self::_STR :
                return is_string($value);
                break;
            case self::_ARRAY : 
                return is_array($value); 
                break;
            case self::_BOOL :
                return gettype($value) == self::_BOOL; 
                break;
        }
    }
}