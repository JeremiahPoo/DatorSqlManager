<?php
require_once 'DatorConnector.php';

/**
 *
 * @author Quentin Gary
 *
 */
class Dator extends DatorConnector {

    const INSERT = "insert";
    const SELECT = "select";
    const UPDATE = "update";

    protected $table;
    protected $param;
    protected $whereQuery;
    protected $orderQuery;
    protected $limitQuery;
    protected $leftJoinQuery;
    protected $groupByQuery;
    protected $debug = true;
    protected $fetchMode = PDO::FETCH_ASSOC;

    protected $queryType;

    /**
     * @param string $table the table of the database
     * @param array $config the credency's for the database
     */
    public function __construct($table, $config) {
        $this->setTable($table);
        $this->setConfig($config);
    }


    /**
     *
     * write a basic select sql query
     * @param array $fields the fields to select
     * @return self
     *
     */
    public function select($fields = array("*")) {

            if(is_string($fields)) {
                $fields = array($fields);
            }
            $this->cleanVar();
            $this->setQueryType(self::SELECT);
            $this->query = "SELECT ".implode(",",$fields)." FROM ".$this->table;
            return $this;
    }
    /**
     * For writing the query
     * @param string $query the query
     * @return self
     */
    public function writeQuery($query) {
        $this->cleanVar();
        $this->query = $query;

        return $this;
    }
    /**
     *
     * make the query to the database
     *
     * @return array $data
     *
     */
    public function makeQuery() {
        if( $this->connect()) {
            $this->setParam();
            $query = $this->query." ".$this->param." ;";
            if($this->debug) {
                echo $query.PHP_EOL;
            }
            $stmt = $this->db->prepare($query);

            if($stmt->execute()) {
                if($this->queryType == self::SELECT) {
                    if(!empty($this->fetchMode)) {
                        $data = $stmt->fetchAll($this->fetchMode);
                    } else {
                        $data = $stmt->fetchAll();
                    }
                    return $data;
                } else {
                    return true;
                }

            } else {
                throw new Exception("Error when executing the query");
            }
        } else {
            throw new Exception("Error when connecting to the database");
        }
    }
    /**
     * set the param string 
     * 
     */
    protected function setParam() {
    
        $this->param = $this->leftJoinQuery." ".$this->whereQuery." ".$this->groupByQuery." ".$this->orderQuery." ".$this->limitQuery;
        
    }
    /**
     *
     * prepare a insert query
     * @param array key value $values : the fields in keys and the values in values
     * @param bool $ignore : if it is a INSERT or a insert ignore
     * @return self
     */
    public function insert($values, $ignore = true) {
        if(is_string($values)) {
            $values = array($values);
        }
        $values = $this->cleanStringArray($values);
        $this->cleanVar();
        $this->setQueryType(self::INSERT);
        $implode = implode(",",array_keys($values));
        $implodeVal = implode(" , ",array_values($values));

        if($ignore) {
            $ignoreStr = "IGNORE";
        } else {
            $ignoreStr = "";
        }
        $this->query = "INSERT ".$ignoreStr." INTO ".$this->table." ( ".$implode." ) VALUES ( ".$implodeVal." ) ";
        return $this;
    }
    /**
     *
     * prepare a update query
     * @param array key value $values : the fields in keys and the values in values
     * @return self
     */
    public function update($values) {
        if(is_string($values)) {
            $values = array($values);
        }
        $this->cleanVar();
        $this->setQueryType(self::UPDATE);
        $params = [];
        foreach($values as $key => $val) {
            $params[] = $key. " = ". $this->cleanString($val);
        }

        $implode = implode(",",$params);

        $this->query = "UPDATE ".$this->table." SET ".$implode." ";
        return $this;
    }

    public function setTable($table) {
        $this->table = $table;
        return $this;
    }

    public function setConfig($config) {
        $this->config = $config;
        return $this;
    }

    public function setDebug($debug) {
        $this->debug = $debug;
        return $this;
    }
    /**
     * add a order by to the query
     *
     * @param string $order : the param to order
     * @param string $sens : the sens of the order (DESC || ASC)
     *
     */
    public function orderBy($order, $sens = null) {
        $this->orderQuery = "";
        if($order != null) {
            $this->orderQuery = "ORDER BY ".$order;
            if($sens != null) {
                $this->orderQuery .= " ".$sens;
            }

         }
         return $this;
    }
    /**
     * add a limit to the query
     *
     * @param int $limit : number of row you want
     * @param int $sens : the start of the query
     *
     * @return self
     *
     */
    public function limit($limit, $start = null) {

        $this->limitQuery = "";

        if($limit != null) {

            if($start != null) {
                $this->limitQuery = "LIMIT ".$start." , ".$limit;
            } else {
                $this->limitQuery = "LIMIT ".$limit;
            }

         }
         return $this;
    }




    /**
     * add a where clause to the query
     *
     * @param array key value $where : key are field , value are value to compare
     * @param bool $strict : if true, the separator will be '=', otherwise it will be 'LIKE'
     * @return self
     *
     *
     */
    public function where($where, $strict = true)
    {

        if($where != null) {
            $str = "";
            $index = 0;

            if(true == $strict) {
                $separator = " = ";
            } else {
                $separator = " LIKE ";
            }

            foreach($where as $key => $value) {

                $str .= $key.$separator.$this->cleanString($value);
                $index ++;
                if($index != count($where) ) {
                    $str .= " AND ";
                }
            }
            $this->setWhere($str);
        }
        return $this;
    }

    /**
     * add a where in clause in the query
     *
     * @param  string $column Column for the where in
     * @param  array  $data   list of data to check
     * @return self
     */
    public function whereIn(string $column, array $data)
    {
        $data = $this->cleanStringArray($data);
        $where = $column . ' IN (' . implode(',', $data) . ')';
        $this->setWhere($where);
        return $this;
    }
    /**
     * Set where value. Initiate start of condition if the attribute is empty
     *
     * @param string  $condition  Condition to add
     * @param boolean $forceReset Reset where value or not
     */
    private function setWhere(string $condition, $forceReset = false)
    {
        if ($this->whereQuery == '' || $forceReset) {
            $this->whereQuery = 'WHERE ' . $condition;
        } else {
            $this->whereQuery = ' ' . $condition;
        }
    }
    /**
     *
     * clean the vars
     *
     */
    protected function cleanVar() {
        $this->whereQuery = "";
        $this->limitQuery = "";
        $this->orderQuery = "";
        $this->leftJoinQuery = "";
        $this->groupByQuery = "";
    }

    /**
     * Get the value of fetchMode
     */
    public function getFetchMode()
    {
        return $this->fetchMode;
    }

    /**
     * Set the value of fetchMode
     *
     * @return  self
     */
    public function setFetchMode($fetchMode)
    {
        $this->fetchMode = $fetchMode;

        return $this;
    }

    /**
     * add a left join to the query
     * @param string $table : the table to join
     * @param string $foreignKey : the foreignKey of local table
     * @param string $field : the field of the join table
     * @return self;
     *
     */
    public function addLeftJoint($table, $foreignKey, $field) {

        $args = func_get_args();

        foreach($args as $arg) {
            if(!is_string($arg)) {
                throw new Exception("All arguments of the function ".__FUNCTION__." must be string");
            }
        }


        $query = "LEFT JOIN ".$table."
        ON ".$this->table.".".$foreignKey." = ".$table.".".$field." ";

        $this->leftJoinQuery .= $query;

        return $this;
    }
    /**
     * remove all leftJoints
     * @return self
     */
    public function cleanLeftJoint() {
        $this->leftJoinQuery = "";

        return $this;
    }
    /**
     * check if the data is a string, if true,
     * addslash
     * @param mixed $data : the data to clean
     * @return mixed : the cleaned data
     */
    public function cleanString($data) {
        if(is_string($data)) {

            //$data = addslashes($data);
            // $data = utf8_decode($data);
            $data = htmlspecialchars($data);
            if($this->connect()) {
                $data = $this->db->quote($data);
            }


        }

        return $data;
    }
    /**
     * apply cleanString on array,
     * @param array $array : the array to clean
     * @param string $mode : assos if associative array, other if normal array
     * @return array : the cleaned array
     */
    public function cleanStringArray($array, $mode = "assos") {
        $cleanedArray = array();

        if($mode == "assos") {
            foreach($array as $key => $value) {
                $cleanedArray[$key] = $this->cleanString($value);
            }
        } else {
            foreach($array as $value) {
                $cleanedArray[] = $value;
            }
        }

        return $cleanedArray;
    }

    /**
     * Get the value of queryType
     */
    public function getQueryType()
    {
        return $this->queryType;
    }

    /**
     * Set the value of queryType
     *
     * @return  self
     */
    public function setQueryType($queryType)
    {
        $this->queryType = $queryType;

        return $this;
    }


    /**
     * for created a group by query 
     * @param mixed $field  
     * @return self
     */
    public function groupBy($field)
    {   
        if(is_array($field)) {
            $field = implode(", ", $field);
        }
        $this->groupByQuery = "group by ".$field;

        return $this;
    }
}