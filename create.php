<?php 
/**
 * script for create a class for database 
 * 
 * ex : for create a class for the database called reviews, 
 * just set the config in config.php and tape in console : 
 * $ create.php --reviews
 * 
 * if you want to use another config, add a second argument 
 * ex : 
 * $ create.php --reviews otherConfigPath.php  
 * 
 */
error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);

$source = "Dator.php.dist";

/**
 * if the second arg is set, we use this path for config. 
 * Otherwise, we use the default config path 
 * 
 */
if(isset($argv[2])) {
    $config = $argv[2];
} else {
    $config = "config.php";
}

$toReplace = [
    "<?php",
    PHP_EOL,
    '$config'
];


if(file_exists($source)) {
    $c = file_get_contents($source);
} else {
    throw new Exception("The file ".$source." doesn't exist or you don't have the permission to access it on line ".__LINE__);
}

if(file_exists($config)) {
    $config = file_get_contents($config);
} else {
    throw new Exception("The file ".$config." doesn't exist or you don't have the permission to access it on line ".__LINE__);
}


$config = str_replace($toReplace,"",$config);
$c = str_replace("¨-)-", $config, $c);


if(substr($argv[1], 0, 2) != "--" || empty($argv[1])) {
    throw new Exception("You have a error on your syntax for create.php. The sytax must be like '".__FILE__." --YOURTABLEE'");
}
$arg = str_replace("--", "",$argv[1]);
$table = $arg;

if(empty($table)) {
    throw new Exception("The argument passed in ".__FILE__." is empty");
}

$c = str_replace("([[]])",$table,$c);

// convert table name like my_table to MyTable for having a beautiful class name 
$tableName = preg_replace_callback("/_([a-z])/",
 function ($matches) {
            return strtoupper($matches[1]);
        }, $table); 

$tableName = ucfirst($tableName);
$dest = $tableName."Dator.php"; 
$c = str_replace("([[tableName]])",$tableName,$c);

if(file_exists($dest)) {
    throw new Exception("The files ".$dest." already exists");
    exit();
}
copy($source,$dest);

if(false === file_put_contents($dest ,$c)) {
    throw new Exception("Error when puttings content into the file.");
} else {
    echo PHP_EOL."The table ".$dest." has been successfully created. ".PHP_EOL;
}