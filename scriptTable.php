<?php 
/**
 * A script for create table with a predefined config 
 * Look README.MD for more information 
 * @author Quentin Gary 
 * 
 */
error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);


if(function_exists("myAutoloader")) {
    spl_autoload_register('myAutoloader');
} else  {
    function myAutoloader($className) {
        $path = __DIR__."/";
        $fileName = $path.$className.'.php';
    
        if(file_exists($fileName)) {
            include $fileName;
        } else {
            $path = __DIR__."/../";
            $fileName = $path.$className.'.php';
    
            if(file_exists($fileName)) {
                include $fileName;
            }
        }
    }
    
    spl_autoload_register('myAutoloader');
}



if(empty($argv[1])) {
    throw new Exception("You need to set the table name as first parameter. ");
}

if(empty($argv[2])) {
    $configPath = "tableConfig.php";
} else {
    $configPath = $argv[2]; 
}

$newTableName = $argv[1];

$columns = include($configPath);

$tableCreate = new TableCreator($newTableName);

foreach($columns as $column) {
    $tableCreate->addColumn(
                    $column->name, 
                    $column->type,
                    $column->length, 
                    $column->default,
                    $column->primary, 
                    $column->auto,
                    $column->foreignKey);
}

if($tableCreate->createTable()) {
    echo "The table ".$newTableName." has been succesfully created in the database.".PHP_EOL;
}

