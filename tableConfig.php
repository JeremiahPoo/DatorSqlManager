<?php 
/**
 * 
 * 
 *  
 *if you want to add foreign key, add a array to the arguments with the name of the other table as arguments
 * and the column you want as target for the foreign key  
*/ 
return (object) array(
    (object)  [
        "name" => "id",
        "type" => "int",
        "length" => 255,
        "default" => null, 
        "primary" => true,
        "auto" => true,
        "foreignKey" => array("testForeign" => "id")
    ],
    (object)  [
        "name" => "column2",
        "type" => "varchar",
        "length" => 255,
        "default" => null, 
        "primary" => false,
        "auto" => false,
        "foreignKey" => array()
    ],
    (object)  [
        "name" => "column3",
        "type" => "date",
        "length" => 0,
        "default" => null, 
        "primary" => false,
        "auto" => false,
        "foreignKey" => array()
    ]


);
